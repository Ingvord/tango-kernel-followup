# Tango Kernel Follow-up Meeting

Held on 2021/02/25 at 15:00 CET on Zoom.

**Participants**:
- Andrew Goetz (ESRF)
- Benjamin Bertrand (MaxIV)
- Lorenzo Pivetta (ELETTRA)
- Michal Liszcz (S2Innovation)
- Reynald Bourtembourg (ESRF)
- Sergi Rubio (ALBA)
- Jairo Moldes (ALBA)
- Thomas Braun (byte physics)
- Vincent Hardion (Max IV)
- Thomas Juerges (LOFAR)
- Anton Joubert (SARAO)
- Sonja Vrcic (SKAO)
- Damien Lacoste (ESRF)

# Minutes

Andy got randomly selected to take notes and write the minutes of this meeting.

## Status of [Actions defined in the previous meetings](../2021-02-25/Minutes.md#summary-of-remaining-actions)

**Action - All institutes**:
Please vote (thumb up reaction in the description of the issue, or e-mail) for the most critical issues you would
like to get solved first.

**Action - Andy (ESRF)**:
Update the description of issue https://github.com/tango-controls/cppTango/issues/822

**Action - Andy (ESRF)**:
Update Documentation to give a recipe on how to install Tango with the Tango Source Distribution on Ubuntu

**Action - Andy (ESRF)**:
Contact Geoff to see when he could present a Kernel Webinar on pybind11.
**The webinar is planned for Wednesday 31st March 2021 at 9:30 CEST. Anton will present pyTango as introduction.  
Action - Andy (ESRF): Contact Geoff to inform him about the webinar and confirm Geoff is available**

**Action - Benjamin B.**:
Try out gitlab runners on windows for pytango

**Action - Benjamin B.**:
Supply script to Piotr for MD-\>HTML conversion used for tango RFCs. **Done**

**Action - Reynald**:
Share the knowledge, after discussing with Emmanuel Taurel, about how the last Tango Windows distribution was created.
https://github.com/tango-controls/TangoTickets/issues/37
**Reynald should upload all the useful scripts he found to build the Tango Source Distribution on the Tango Source 
Distribution repository**

**Action - Reynald**:
Create ticket in cppTango for RelWithDebInfo, see CMAKE_BUILD_TYPE, https://cmake.org/cmake/help/latest/variable/CMAKE_BUILD_TYPE.html
**Done : https://gitlab.com/tango-controls/cppTango/-/issues/839**

**Action - Reynald**:
Look at old cppTango PRs targeting `tango-v10`, and comment/close/create issue

**Action - Sergi**:
tangocpp PR 742 (on names on logs) to be reviewed

**Action - Sergi**: debug a memory leak issue when using pytango 9.3.3 and older when reading states of devices in a thread.
Ongoing, (note: please add a issue link)

**Action - Sergi**:
Organize a meeting with Anton, Zibi and Sergi on the DeviceProxy destruction topic. Investigation is still ongoing.

**Action - Thomas B.**:
Gitlab migration:
* https://github.com/tango-controls/tango_admin
* https://github.com/tango-controls/TangoDatabase
* https://github.com/tango-controls/starter
* https://github.com/tango-controls/docker-mysql
* https://github.com/tango-controls/TangoTest-docker
* https://github.com/tango-controls/tango-libs-docker

See also https://github.com/tango-controls/TangoTickets/issues/47
**These repositories have been migrated. The default branches have been renamed from master to main.  
Thomas used a script to update the MR target branches to main**

**Action - Thomas B.**:
Try out the gitlab runners on windows which are in beta phase from gitlab

**Action - Thomas B.**:
Rename cppTango branches and update Readme.md/HowToContribute.md
**Done, using a script to update the MR target branches from to main.

**Action - Thomas B.**:
Remove "Ready for Merge" label and convert existing MRs into draft mode. Document how we use these labels in
HowToContribute.md
**Done**

**Action - Vincent**:
Reach out to colleagues for presenting pytango on tango kernel webinar.
**This action was the result of a misunderstanding.  
It was simply said in the previous meeting that Vincent will see with his colleagues to see whether some of them could 
contribute to pytango.**

## [Tango RFCs]

See https://gitlab.com/tango-controls/rfc/-/wikis/Meeting-2021-02-25

## Gitlab migration

The following repositories have been migrated during the last 2 weeks (thanks mainly to Thomas Braun):
- rfc
- tango_admin
- TangoDatabase
- starter
- docker-mysql
- TangoTest-docker
- tango-libs-docker
- tango-cs-docker
- tango-libs-dependencies-docker
- TangoAccessControl
- tango-idl
- TangoTest

For cppTango-docs, Thomas created issue https://gitlab.com/tango-controls/cppTango/-/issues/837 in cppTango to find a 
new location to host the cppTango doxygen documentation.

For JTango Krystian (S2Innovation) is currently working on migrating the CI to gitlab.

Conda recipes repositories won't be migrated to gitlab since they are now on conda-forge github organization.

**Action - Reynald**: Contact Igor to coordinate the migration to Gitlab of the repositories he's administrating:
rest-server, rest-test-suite, rest-api-java, tango-camel, tango-controls-demo, jtango-maven-archetype

**Action -Reynald**: Contact his ESRF colleagues and coordinate the migration of the repositories they are currently 
administrating (pogo, SettingsManager, atk, matlab-binding, jupyTango, igorpro-binding, labview-binding, JSSHTerminal,
Astor, jive, LogViewer, atk-panel, C_Binding, atk-tuning, ForwardedComposer, wunderground, DBBench)

**Action - Vincent**: Ensure rest-api repository can be migrated smoothly and coordinate the migration with the Gitlab 
migration team (Benjamin could help).

**Action - Thomas Braun**: Start the migration process for TangoTickets repository.

**Action - Benjamin**: Migrate tango-kernel-followup repository

**Action - Sergi**: Organize the migration of the repositories he's administrating (fandango, PANIC, simulatorDS, VACCA).
PyTangoArchiving will have to migrated under hdbpp subgroup.

**Action - S2Innovation**: Organize the migration of the repositories they are administrating 
(tango-doc, dsc, sys-tango-benchmark, sys-tango-benchmark-standard-tests, dsc-import, tangobox).

## High Priority issues

In pyTango, the priority is to make the pipelines reliable again.
Some tests which used to pass no longer pass.

Some optimizations could be implemented to use prebuilt docker images to make the tests execution faster.

In pytango, the 2 main issues are pytango!409 and pytango!371 which are issues with the events when using a FileDatabase.

**Action - Thomas Braun**: Try to reproduce the issues pytango#409 and pytango#371 and try to identify the origin of the 
problem (pytango or cpptango?) and to find a solution to avoid the segmentation fault on the client side.

Vincent asked whether it would would be possible to mock the Database Object used in Tango for unit tests.

Thomas Braun and Michal reminded about https://gitlab.com/tango-controls/TangoDatabase/-/issues/5 issue which was created in order 
to study the possibility to get interchangeable Database backend. A file based backend (one could implement an SQLite backend) 
could be developed.

For the unit tests, Vincent would prefer to have a lighter solution where no database server would be required to run the tests.

The current documentation says that events are not expected to work properly when not using a database server.
If we keep it like that, client trying to subscribe to events in this use case should get an exception during the 
event subscription.

## AOB
### conda recipes

Work is ongoing to create a separate cppTango package which hold debug symbols. https://github.com/conda-forge/cpptango-feedstock/pull/2
Benjamin will propose a solution to overcome the CRC Checksum problem encountered during the tests.

Benjamin suggested to create conda packages for the java tools.
Thomas Braun warned that there might be some Licensing issues with JacORB and CORBA classes in java. 
This was a reason for not having official Debian packages for the java tools.
Benjamin said that we only need to ensure that the license allows redistribution of the software. 
It does not have to be a free software license. The conda package can probably be distributed under multiple licenses 
(free license for the Tango SW and the original licenses for the JacORB and java CORBA classes).

**Action - Benjamin**: Investigate on whether it would be possible to create conda packages for the Tango java tools.
Need to check whether the JacORB license and java CORBA classes allow to redistribute freely the software.

### Next teleconf meeting

Tango Kernel Teleconf Meetings take place on the 2nd and 4th Thursday of each month, at 15:00 CET or CEST (Paris time).

So the next Tango Kernel Teleconf Meeting will take place on Thursday 11th March 2021 at 15:00 **CET**.

## Summary of remaining actions

**Action - All institutes**:
Please vote (thumb up reaction in the description of the issue, or e-mail) for the most critical issues you would
like to get solved first.

**Action - Andy (ESRF)**:
Update the description of issue https://github.com/tango-controls/cppTango/issues/822

**Action - Andy (ESRF)**:
Update Documentation to give a recipe on how to install Tango with the Tango Source Distribution on Ubuntu

**Action - Andy (ESRF)**: Contact Geoff to inform him about the webinar and confirm Geoff is available

**Action - Anton**: Prepare first part of the coming Tango Kernel Webinar (31st March 9:30 CEST) on pyTango.

**Action - Benjamin**: Investigate on whether it would be possible to create conda packages for the Tango java tools.
Need to check whether the JacORB license and java CORBA classes allow to redistribute freely the software.

**Action - Benjamin**: Migrate tango-kernel-followup repository

**Action - Benjamin B.**:
Try out gitlab runners on windows for pytango

**Action - Reynald**: Contact Igor to coordinate the migration to Gitlab of the repositories he's administrating:
rest-server, rest-test-suite, rest-api-java, tango-camel, tango-controls-demo, jtango-maven-archetype

**Action -Reynald**: Contact his ESRF colleagues and coordinate the migration of the repositories they are currently
administrating (pogo, SettingsManager, atk, matlab-binding, jupyTango, igorpro-binding, labview-binding, JSSHTerminal,
Astor, jive, LogViewer, atk-panel, C_Binding, atk-tuning, ForwardedComposer, wunderground, DBBench)

**Action - Reynald**:
Share the knowledge, after discussing with Emmanuel Taurel, about how the last Tango Windows distribution was created.
https://github.com/tango-controls/TangoTickets/issues/37
**Reynald should upload all the useful scripts he found to build the Tango Source Distribution on the Tango Source
Distribution repository**

**Action - Reynald**:
Look at old cppTango PRs targeting `tango-v10`, and comment/close/create issue

**Action - Sergi**: Organize the migration of the repositories he's administrating (fandango, PANIC, simulatorDS, VACCA).
PyTangoArchiving will have to migrated under hdbpp subgroup.

**Action - Sergi**:
cpptango#742 (on names on logs) to be reviewed

**Action - Sergi**: debug a memory leak issue when using pytango 9.3.3 and older when reading states of devices in a thread.
Ongoing, (note: please add a issue link)

**Action - Thomas Braun**: Try to reproduce the issues pytango#409 and pytango#371 and try to identify the origin of the
problem (pytango or cpptango?) and to find a solution to avoid the segmentation fault on the client side.

**Action - Thomas Braun**: Start the migration process for TangoTickets repository.

**Action - Thomas B.**:
Try out the gitlab runners on windows which are in beta phase from gitlab

**Action - Vincent**: Ensure rest-api repository can be migrated smoothly and coordinate the migration with the Gitlab
migration team (Benjamin could help).

**Action - S2Innovation**: Organize the migration of the repositories they are administrating
(tango-doc, dsc, sys-tango-benchmark, sys-tango-benchmark-standard-tests, dsc-import, tangobox).
