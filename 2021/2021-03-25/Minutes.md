# Tango Kernel Follow-up Meeting

Held on 2021/03/25 at 15:00 CET on Zoom.

**Participants**:

- Alberto Lopez (ALBA)
- Andy Gotz (ESRF)
- Anton Joubert (SARAO)
- Benjamin Bertrand (MaxIV)
- Damien Lacoste (ESRF)
- Lorenzo Pivetta (ELETTRA)
- Michał Liszcz (S2Innovation)
- Nicolas Leclercq (ESRF)
- Sergi Rubio (ALBA)
- Thomas Braun (byte physics)
- Thomas Juerges (LOFAR)
- Vincent Hardion (Max IV)

## Minutes

Thomas B. volunteered to take notes and write the minutes of this meeting.

### Status of [Actions defined in the previous meetings](../2021-03-11/Minutes.md#summary-of-remaining-actions)

**Action - All institutes**:
Please vote (thumb up reaction in the description of the issue, or e-mail) for the most critical issues you would like to get solved first.

**Action - Andy (ESRF)**:
Update the description of issue <https://github.com/tango-controls/cppTango/issues/822>

**Action - Andy (ESRF)**:
Update Documentation to give a recipe on how to install Tango with the Tango Source Distribution on Ubuntu

**Action - Andy (ESRF)**: Contact Geoff to inform him about the webinar and confirm Geoff is available
**Not yet confirmed**

**Action - Andy (ESRF)**: Contact freexian about maintaining omniORB packages in debian

**Action - Anton**:
Prepare first part of the coming Tango Kernel Webinar (31st March 9:30 CEST) on pyTango.
**Will be moved to 14th April 9:30 CEST**

**Action - Benjamin**:
Investigate on whether it would be possible to create conda packages for the Tango java tools.
Need to check whether the JacORB license and java CORBA classes allow to redistribute freely the software.
JacORB is under LGPL: <https://www.jacorb.org/lgpl-explained.html>.
Reynald proposed to contact Frederic Picca to get more information about license issue with Debian packages.
**Work-In-Progress: Might require proprietary license as idl/omg/CosBridgeAdmin.idl is not under
LGPL-v2.**

**Action - Benjamin B.**:
Try out gitlab runners on windows for pytango

**Action - Benjamin B.**:
Check if C++11 can be used at MAX IV and contact Anton.
**Yes C++11 is fine**

**Action - Reynald**:
Contact Igor to coordinate the migration to Gitlab of the repositories he's administrating:
rest-server, rest-test-suite, rest-api-java, tango-camel, tango-controls-demo, jtango-maven-archetype
E-mail sent. No intention to migrate to GitLab for now. Is it mandatory to move? The consensus is that for consistency
every repo should be moved. Will wait for his feedback and see again when all other repos have been moved.
**Unused repos are archived on github, the rest will stay at github for now as there is no value for moving to gitlab
   according to Igor.**

**Action - Reynald**:
Contact his ESRF colleagues and coordinate the migration of the repositories they are currently
administrating (pogo, SettingsManager, matlab-binding, jupyTango, igorpro-binding, labview-binding, JSSHTerminal,
Astor, jive, LogViewer, C_Binding, atk-tuning, ForwardedComposer, wunderground, DBBench)

**Action - Reynald**:
Share the knowledge, after discussing with Emmanuel Taurel, about how the last Tango Windows distribution was created.
<https://github.com/tango-controls/TangoTickets/issues/37>
Reynald should upload all the useful scripts he found to build the Tango Source Distribution on the Tango Source Distribution repository.
In Progress. MR created after the meeting: <https://gitlab.com/tango-controls/TangoSourceDistribution/-/merge_requests/100>
**Done**

**Action - Reynald**:
Look at old cppTango PRs targeting `tango-v10`, and comment/close/create issue

**Action - Reynald**:
Contact Frederic Picca to get more information about JacORB license issue with Debian packages.

**Action - Reynald**:
Contact Jean-Michel about mailing list issue

**Action - Sergi**: Organize the migration of the repositories he's administrating (fandango, PANIC, simulatorDS, VACCA).
PyTangoArchiving will have to migrated under hdbpp subgroup.

**Action - Sergi**:
cpptango#742 (on names on logs) to be reviewed

**Action - Thomas B.**: Try to reproduce the issues pytango#409 and pytango#371 and try to identify the origin of the
problem (pytango or cpptango?) and to find a solution to avoid the segmentation fault on the client side. 409 is a
cppTango bug. Will pursue further.
**pytango#409 could be reproduced, it is a cppTango bug. Will pursue further.**

**Action - Thomas B.**: Start the migration process for TangoTickets repository.
Issue created: <https://github.com/tango-controls/TangoTickets/issues/53>

**Action - Thomas B.**:
Try out the gitlab runners on windows which are in beta phase from gitlab

**Action - Vincent**: Ensure rest-api repository can be migrated smoothly and coordinate the migration with the Gitlab
migration team (Benjamin could help).

**Action - S2Innovation**: Organize the migration of the repositories they are administrating
(dsc, dsc-import, tangobox).

### Tango Meeting

The in-person in meeting in St. Petersburg might not happen in June due to C****** related restrictions. But no decision
yet.

### Tango Collaboration Contract

Discussion of who is project leader for the core tango projects. Many current leaders retire soon. Required for
paperwork. Added some more projects to the list Andy sent to the mailing list.

### Tango Kernel Webinars

One regarding pytango will happen on 14th April 9:30 CEST.

#### Next teleconf meeting

Tango Kernel Teleconf Meetings take place on the 2nd and 4th Thursday of each month, at 15:00 CET or CEST (Paris time).

So the next Tango Kernel Teleconf Meeting will take place on Thursday 8th April 2021 at 15:00 **CEST**.

## Summary of remaining actions

**Action - All institutes**:
Please vote (thumb up reaction in the description of the issue, or e-mail) for the most critical issues you would like to get solved first.

**Action - Andy (ESRF)**:
Update the description of issue <https://github.com/tango-controls/cppTango/issues/822>

**Action - Andy (ESRF)**:
Update Documentation to give a recipe on how to install Tango with the Tango Source Distribution on Ubuntu

**Action - Andy (ESRF)**:
Contact Geoff to inform him about the webinar and confirm Geoff is available

**Action - Andy (ESRF)**:
Contact freexian about maintaining omniORB packages in debian

**Action - Benjamin B.**:
Investigate on whether it would be possible to create conda packages for the Tango java tools.
Need to check whether the JacORB license and java CORBA classes allow to redistribute freely the software.
JacORB is under LGPL: <https://www.jacorb.org/lgpl-explained.html>.
Might require proprietary license as idl/omg/CosBridgeAdmin.idl is not under LGPL-v2.

**Action - Benjamin B.**:
Try out gitlab runners on windows for pytango

**Action - Reynald**:
Contact his ESRF colleagues and coordinate the migration of the repositories they are currently
administrating (pogo, SettingsManager, matlab-binding, jupyTango, igorpro-binding, labview-binding, JSSHTerminal,
Astor, jive, LogViewer, C_Binding, atk-tuning, ForwardedComposer, wunderground, DBBench)

**Action - Reynald**:
Look at old cppTango PRs targeting `tango-v10`, and comment/close/create issue

**Action - Reynald**:
Contact Jean-Michel about mailing list issue

**Action - Sergi**:
Organize the migration of the repositories he's administrating (fandango, PANIC, simulatorDS, VACCA).
PyTangoArchiving will have to migrated under hdbpp subgroup.

**Action - Sergi**:
cpptango#742 (on names on logs) to be reviewed. Michał is reworking the MR in response to Sergi's feedback.

**Action - Thomas B.**:
Try to reproduce the issues pytango#409 and pytango#371 and try to identify the origin of the
problem (pytango or cpptango?) and to find a solution to avoid the segmentation fault on the client side. pytango#409 is a
cppTango bug, needs a proper fix.

**Action - Thomas B.**:
Start the migration process for TangoTickets repository, see [here](https://github.com/tango-controls/TangoTickets/issues/53).

**Action - Thomas B.**:
Try out the gitlab runners on windows which are in beta phase from gitlab

**Action - Vincent**: Ensure rest-api repository can be migrated smoothly and coordinate the migration with the Gitlab
migration team (Benjamin could help).

**Action - S2Innovation**: Organize the migration of the repositories they are administrating
(dsc, dsc-import, tangobox).
