# Tango Kernel Follow-up Meeting - 2020/04/23

To be held on 2020/04/23 at 15:00 CEST on Zoom.

# Agenda
 
 1. Status of [Actions defined in the previous meetings](https://github.com/tango-controls/tango-kernel-followup/blob/master/2020/2020-04-09/Minutes.md#summary-of-remaining-actions)
 2. cppTango News & next PR merge goals
 3. JTango News
 4. PyTango News
 5. Tango Source Distribution News
 6. Conda packages
 7. Tango Meeting 2020
 8. High priority issues
 9. AOB
     - HDB++