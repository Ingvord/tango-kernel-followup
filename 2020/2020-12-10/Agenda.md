# Tango Kernel Follow-up Meeting - 2020/12/10

To be held on 2020/12/10 at 15:00 CET on Zoom.

# Agenda
 
 1. Status of [Actions defined in the previous meetings](https://github.com/tango-controls/tango-kernel-followup/blob/master/2020/2020-11-26/Minutes.md#summary-of-remaining-actions)
 2. [Tango RFCs](https://github.com/tango-controls/rfc/wiki/Meeting-2020-12-10)
 3. Travis-ci.org shutdown ([cppTango#812](https://github.com/tango-controls/cppTango/issues/812))
 4. High priority issues
 5. Tango Kernel Webinars
 6. AOB
