# Tango Kernel Follow-up Meeting - 2020/06/25

To be held on 2020/06/25 at 15:00 CEST on Zoom.

# Agenda
 
 1. Status of [Actions defined in the previous meetings](https://github.com/tango-controls/tango-kernel-followup/blob/master/2020/2020-06-11/Minutes.md#summary-of-remaining-actions)
 2. cppTango News
 3. JTango News
 4. PyTango News
 5. Tango Source Distribution News
 6. Conda packages
 7. High priority issues
 8. AOB
     - HDB++ (Reminder: HDB++ teleconf meeting on 2020/06/26 at 14:00 CEST)